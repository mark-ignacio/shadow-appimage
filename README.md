# Shadow AppImage project

This project builds an AppImage to provide support for all system for the application Shadow Linux. The software is not designed by us, it is the property of Blade (https://shadow.tech/).

The main GitHub of the Linux community for Shadow is available [here](https://github.com/NicolasGuilloux/blade-shadow-beta/). Please, report all bugs found in the [Linux community GitHub](https://github.com/NicolasGuilloux/blade-shadow-beta/issues) instead of this one.

## Download

Stable version: https://nicolasguilloux.github.io/blade-shadow-beta/#appimage

Nightly version: https://gitlab.com/aar642/shadow-appimage/-/jobs/artifacts/master/raw/shadowbeta-linux-x86_64.AppImage?job=bionic_build

## Discord servers

  Find us on Shadow Official Discord servers!

- [Discord Shadow FR](https://discordapp.com/invite/shadowtech)
- [Discord Shadow UK](https://discordapp.com/invite/ShadowEN)
- [Discord Shadow DE](https://discord.gg/shadowde)
- [Discord Shadow US](https://shdw.me/USDiscord)
- [Discord Shadow Community Projects](https://discord.gg/9HwHnHq)

## Maintainers
![Alex^#1629](https://cdn.discordapp.com/avatars/401575828590428161/36d0ac43c2cb3a72d41c51b0c8375f65.png?size=64 "Alex^#1629")
![GiantPandaRoux#0777](https://cdn.discordapp.com/avatars/267044035032514561/e98147b99f4821c4e806e97fda05e69a.png?size=64 "GiantPandaRoux#0777")
![Nover#9563](https://cdn.discordapp.com/avatars/248726456551604224/4f22c1d6e37874987470c1af7dc21d10.png?size=64 "Nover#9563")

## Disclaimer

This is a community project, project is not affliated to Blade in any way.

[Shadow](https://shadow.tech) logo and embeded Linux client is property of [Blade Group](http://www.blade-group.com/).
